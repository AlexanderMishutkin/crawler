from urllib.parse import urlparse

from bs4 import BeautifulSoup


def find_all_hrefs(html):
    """
    Simple select of all <a>'s hrefs in an html file.
    """
    links = BeautifulSoup(html, "html.parser").find_all('a')
    links = [link.get('href') for link in links]
    if None in links:
        links.remove(None)
    return links


def get_netloc(link, home=None, top_domain_only=False):
    """
    Returns webpage host without any additional data.

    Set up home hostname with urlparse to map relative URLs into absolute ones.
    Set top_domain_only to True to ignore subdomains.
    """
    parsed = urlparse(link)

    # As it stated in URL standard - netloc should starts with "//". So sometimes we must add slashes to parse it.
    if parsed.path != "" and parsed.path[0:1] != '/':
        parsed = urlparse("//" + link)
    if parsed.netloc == "" and home:
        if top_domain_only:
            return '.'.join(home.netloc.split('.')[-2:])
        return home.netloc
    if top_domain_only:
        return '.'.join(parsed.netloc.split('.')[-2:])
    return parsed.netloc


def classify_hrefs(home_hostname, links, keep_anchor=False, by_subdomains=False):
    """
    Builds a dictionary, mapping hostnames to lists of unique links to that hostname.

    home_hostname is needed to work with relative URLs.
    Set keep_anchor to True to keep it in the result's URLs.
    By default, only top level domain is used as dict key. Set by_subdomains to True to change this logic.
    """
    home_hostname = urlparse(home_hostname)
    links_by_netloc = {}
    for link in links:
        if link is not None:
            key = get_netloc(link, home_hostname, not by_subdomains)
            if key not in links_by_netloc:
                links_by_netloc[key] = set()
            to_add = link.split("#")[0] if not keep_anchor else link  # Removing anchor
            to_add = to_add[2:] if to_add[:2] == "//" else to_add  # Removing trailing slashes
            links_by_netloc[key].add(to_add)
    for key in links_by_netloc:
        links_by_netloc[key] = sorted(list(links_by_netloc.get(key)))
    return links_by_netloc
