from django import forms


class CrawlerForm(forms.Form):
    target = forms.URLField(label='Webpage to parse...', required=True)
