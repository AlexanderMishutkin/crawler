# Crawler
Django pet project.

This program took webpage as an argument and returns a list of all links from those page.

### Solution and project structure
The solution consists of one application with two views: 
`crawler` and `crawler_form`.

[Crawler Form view](/crawler_app/views.py) is responsible for receiving and validating URL by using Django [form](/crawler_app/forms.py).
Then it redirects user to another one.

[Crawl view](/crawler_app/views.py) do parsing and render result as [a table](/crawler_app/templates/crawler-results.html), 
where all links are listed and group by hosts.
This view is cached, cause it use heavy evaluations and is under the risk of being banned for multiple requests.

Parsing is done by [parser.py](/crawler_app/parser.py). `find_all_hrefs` method is responsible for parsing webpages with **bs4**.
While `classify_hrefs` groups links by domains.

### Unclosed questions
1. Should I ignore URL anchors?
2. How to combine async views with sync cache?

### TO DO in production
1. Add Captcha.
2. Make `crawl` view asynchronous.
3. Change **bs4** engine more powerful one.
4. Of course, replace Django interface with some React pages.
``