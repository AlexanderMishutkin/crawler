from .forms import CrawlerForm
from .parser import find_all_hrefs, classify_hrefs, get_netloc

from django.views.decorators.cache import cache_page
from django.http import HttpResponseNotFound, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render
from urllib import parse
import requests


def crawler_form(request):
    """
    This form is made to validate the URL. No parsing or other heavy calculations are done here.

    It is not cached, those a good point to measure business statistics.
    Later it could be fully replaced by static frontend.
    """
    if request.method == 'POST':
        form = CrawlerForm(request.POST)
        if form.is_valid():
            url_component = parse.quote(form.cleaned_data['target'], safe='')
            success_url = reverse(crawl, kwargs={'url': url_component})
            return HttpResponseRedirect(success_url)
    else:
        form = CrawlerForm(initial={'target': 'https://'})
    return render(request, 'crawler-form.html', {'form': form})


@cache_page(60 * 15)
def crawl(request, url):
    """
    This view make request to the target host and returns a list of URLs rendered as a table.

    It's the main logic of the application.
    This view can be easily turned into Restful API: just return render context as standalone JSON file.
    """

    rec_check = 'Crawl-recursion-check'
    headers = {rec_check: 'ON'}
    # Special header will stop the app from trying to parse itself.
    if request.headers.get(rec_check, "OFF") == "ON":
        return HttpResponseNotFound()
    target_url = parse.unquote(url)

    # TODO: Implement asynchronous request.
    # The problem is that per-view caching do not support it, so async per-object cache must be used.

    # async def async_request():
    #     return requests.get(target_url, headers=headers)
    # result = await async_request()

    result = requests.get(target_url, headers=headers)

    if result.status_code != requests.codes.ok or 'text/html' not in result.headers['content-type']:
        return HttpResponseNotFound('<h2>URL returns non-HTML answer</h2>')

    links = classify_hrefs(target_url, find_all_hrefs(result.text))
    # Links are divided by their domain.
    # However, links without netloc part ("/only/path") could not be used. Below we will add a netloc part to them.
    target_netloc = get_netloc(target_url)
    target_domain = get_netloc(target_url, top_domain_only=True)
    local_links = [
                      target_netloc + link for link in links.get(target_domain, []) if get_netloc(link) == ""
                  ] + [
                      link for link in links.get(target_domain, []) if get_netloc(link) != ""
                  ]
    if target_domain in links:
        del links[target_domain]

    # Local links have the same domain as a target URL
    return render(request, 'crawler-results.html',
                  {
                      'url': target_url,
                      'local_links': local_links,
                      'global_links': links,
                  })
